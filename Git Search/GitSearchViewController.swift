//
//  GitSearchViewController.swift
//  Git Search
//
//  Created by Hussain KMR Behestee on 2022/03/13.
//

import UIKit

class GitSearchViewController: UITableViewController, UISearchBarDelegate {

    // Interface binding
    @IBOutlet weak var searchBar: UISearchBar!
    
    // search result bising with table source
    fileprivate var repositories: [Repository] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = repositories[indexPath.row].fullName
        if let description = repositories[indexPath.row].description {
            cell.detailTextLabel?.text = description
        }
        let starLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
        starLabel.text = "★\(self.repositories[indexPath.row].stargazersCount)"
        starLabel.textAlignment = .left
        starLabel.sizeToFit()
        starLabel.textColor = UIColor.gray
        cell.accessoryView = starLabel

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        UIApplication.shared.open(repositories[indexPath.row].htmlUrl, options: [:], completionHandler: nil)
    }
    
    //MARK: Searchbar implementation
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Trottle implementing.
        self.timer?.invalidate()
        self.timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.search), userInfo: nil, repeats: false)
    }
    
    @objc func search() {
        self.repositories.removeAll()
        guard let query = self.searchBar.text else { return }
        if query == "" { return }
        searchBar.isLoading = true
        SearchRepository(query: query).request { (result) in
            switch result {
            case .success(let response):
                DispatchQueue.main.async {
                    self.searchBar.isLoading = false
                    self.repositories.append(contentsOf: response.items)
                }
            case .failure(let error):
                self.searchBar.isLoading = false
                print(error.localizedDescription)
            }
        }
    }
    
    
    
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension UISearchBar {

    public var textField: UITextField? {
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            let subViews = subviews.flatMap { $0.subviews }
            guard let textField = (subViews.filter { $0 is UITextField }).first as? UITextField else {
                return nil
            }
            return textField
        }
    }

    public var activityIndicator: UIActivityIndicatorView? {
        return textField?.leftView?.subviews.compactMap{ $0 as? UIActivityIndicatorView }.first
    }

    var isLoading: Bool {
        get {
            return activityIndicator != nil
        } set {
            if newValue {
                if activityIndicator == nil {
                    let newActivityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
                    newActivityIndicator.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
                    newActivityIndicator.startAnimating()
                    //newActivityIndicator.backgroundColor = UIColor.white
                    newActivityIndicator.color = UIColor.black
                    textField?.leftView?.addSubview(newActivityIndicator)
                    let leftViewSize = textField?.leftView?.frame.size ?? CGSize.zero
                    newActivityIndicator.center = CGPoint(x: leftViewSize.width/2, y: leftViewSize.height/2)
                }
            } else {
                activityIndicator?.removeFromSuperview()
            }
        }
    }
}
