//
//  JSONObject.swift
//  Git Search
//
//  Created by Hussain KMR Behestee on 2022/03/13.
//

import Foundation

protocol JSONDecodable {
    init(JSON: JSONObject) throws
}

/**
 Error output if not what you expect when trying to get a value from JSON

 - MissingRequiredKey:   The required key does not exist
 - UnexpectedType:       Value is not the expected type
 - UnexpectedValue:      The value is not what you expect
 */
enum JSONDecodeError: Error, CustomDebugStringConvertible {
    case missingRequiredKey(String)
    case unexpectedType(key: String, expected: Any.Type, actual: Any.Type)
    case unexpectedValue(key: String, value: Any, message: String?)

    var debugDescription: String {
        switch self {
        case .missingRequiredKey(let key):
            return "JSON Decode Error: Required key '\(key)' missing"
        case let .unexpectedType(key: key, expected: expected, actual: actual):
            return "JSON Decode Error: Unexpected type '\(actual)' was supplied for '\(key): \(expected)'"
        case let .unexpectedValue(key: key, value: value, message: message):
            return "JSON Decode Error: \(message ?? "Unexpected value") '\(value)' was supplied for '\(key)'"
        }
    }
}

/**
 *  A type that converts a JSON value
 */
protocol JSONValueConverter {
    associatedtype FromType
    associatedtype ToType

    func convert(key: String, value: FromType) throws -> ToType
}

/**
 *  Do not convert
 */
struct DefaultConverter<T>: JSONValueConverter {
    typealias FromType = T
    typealias ToType = T

    func convert(key: String, value: FromType) -> DefaultConverter.ToType {
        return value
    }
}

/**
 *  Convert a JSON object to some JSONDecodable type
 */
struct ObjectConverter<T: JSONDecodable>: JSONValueConverter {
    typealias FromType = [String: AnyObject]
    typealias ToType = T

    func convert(key: String, value: FromType) throws -> ObjectConverter.ToType {
        return try T(JSON: JSONObject(JSON: value))
    }
}

/**
 *  Convert an array of JSON to an array of some JSONDecodable type
 */
struct ArrayConverter<T: JSONDecodable>: JSONValueConverter {
    typealias FromType = [[String: AnyObject]]
    typealias ToType = [T]

    func convert(key: String, value: FromType) throws -> ArrayConverter.ToType {
        return try value.map(JSONObject.init).map(T.init)
    }
}

/**
 *  JSON primitive value
 */
protocol JSONPrimitive {}

extension String: JSONPrimitive {}
extension Int: JSONPrimitive {}
extension Double: JSONPrimitive {}
extension Bool: JSONPrimitive {}

/**
 *  Types that can be converted using JSONValueConverter
 */
protocol JSONConvertible {
    associatedtype ConverterType: JSONValueConverter
    static var converter: ConverterType { get }
}

/**
 *  A wrap object to get some type of value from a JSON object
 *  get(_:)  Overload selects the appropriate method
 */
struct JSONObject {

    // Dictionary of the original JSON object
    let JSON: [String: AnyObject]

    init(JSON: [String: AnyObject]) {
        self.JSON = JSON
    }

    func get<Converter: JSONValueConverter>(_ key: String, converter: Converter) throws -> Converter.ToType {
        guard let value = JSON[key] else {
            throw JSONDecodeError.missingRequiredKey(key)
        }
        guard let typedValue = value as? Converter.FromType else {
            throw JSONDecodeError.unexpectedType(key: key, expected: Converter.FromType.self, actual: type(of: value))
        }
        return try converter.convert(key: key, value: typedValue)
    }

    func get<Converter: JSONValueConverter>(_ key: String, converter: Converter) throws -> Converter.ToType? {
        guard let value = JSON[key] else {
            return nil
        }
        if value is NSNull {
            return nil
        }
        guard let typedValue = value as? Converter.FromType else {
            throw JSONDecodeError.unexpectedType(key: key, expected: Converter.FromType.self, actual: type(of: value))
        }
        return try converter.convert(key: key, value: typedValue)
    }

    func get<T: JSONPrimitive>(_ key: String) throws -> T {
        return try get(key, converter: DefaultConverter())
    }

    func get<T: JSONPrimitive>(_ key: String) throws -> T? {
        return try get(key, converter: DefaultConverter())
    }

    func get<T: JSONConvertible>(_ key: String) throws -> T where T == T.ConverterType.ToType {
        return try get(key, converter: T.converter)
    }

    func get<T: JSONConvertible>(_ key: String) throws -> T? where T == T.ConverterType.ToType {
        return try get(key, converter: T.converter)
    }

    func get<T: JSONDecodable>(_ key: String) throws -> T {
        return try get(key, converter: ObjectConverter())
    }

    func get<T: JSONDecodable>(_ key: String) throws -> T? {
        return try get(key, converter: ObjectConverter())
    }

    func get<T: JSONDecodable>(_ key: String) throws -> [T] {
        return try get(key, converter: ArrayConverter())
    }

    func get<T: JSONDecodable>(_ key: String) throws -> [T]? {
        return try get(key, converter: ArrayConverter())
    }

}

// MARK: - Expansion for Foundation
// Allows you to create NSURLs and NSDates

import Foundation

extension URL: JSONConvertible {
    typealias ConverterType = URLConverter
    static var converter: ConverterType {
        return URLConverter()
    }
}

extension Date: JSONConvertible {
    typealias ConverterType = DateConverter
    static var converter: ConverterType {
        return DateConverter()
    }
}

struct URLConverter: JSONValueConverter {
    typealias FromType = String
    typealias ToType = URL

    func convert(key: String, value: FromType) throws -> URLConverter.ToType {
        guard let URL = URL(string: value) else {
            throw JSONDecodeError.unexpectedValue(key: key, value: value, message: "Invalid URL")
        }
        return URL
    }
}

struct DateConverter: JSONValueConverter {
    typealias FromType = TimeInterval
    typealias ToType = Date

    func convert(key: String, value: FromType) -> DateConverter.ToType {
        return Date(timeIntervalSince1970: value)
    }
}
